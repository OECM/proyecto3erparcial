﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    
    private PizzaFacade P0;
    private Pizza PComparar;

    Pizza P1 = new PizzaBuilder("Pepperoni").setPan().setSalsa().setQueso().setPepperoni().build();
    Pizza P2 = new PizzaBuilder("Hawaiana").setPan().setSalsa().setQueso().setJamon().setPina().build();
    Pizza P3 = new PizzaBuilder("Vegetariana").setPan().setSalsa().setAceitunas().setChampinones().setJalapeno().setPimiento().build();
    Pizza P4 = new PizzaBuilder("SuperMeaty").setPan().setSalsa().setQueso().setPepperoni().setJamon().setTocino().setSalchicha().build();
    Pizza P5 = new PizzaBuilder("Mexicana").setPan().setSalsa().setQueso().setPepperoni().setJamon().setJalapeno().setPimiento().setTocino().setAceitunas().setChampinones().setSalchicha().build();

    public GameObject pan;
    public GameObject salsa;
    public GameObject queso;
    public GameObject pepperoni;
    public GameObject tocino;
    public GameObject pimento;
    public GameObject champinones;
    public GameObject jamon;
    public GameObject pina;
    public GameObject salchicha;
    public GameObject jalapeno;
    public GameObject aceitunas;

    public Text Orden;
    public Text Cont;
    public int Contador;
    public GameObject Menu;

    int op = 0;



    void Start()
    {
        PComparar = P1;
        P0 = new PizzaFacade();
    }

    private void Update()
    {
        Orden.text = "Orden: " + PComparar.Nombre;
        Cont.text = "Puntuacion: " + Contador;

        //if(Input.GetKey(KeyCode.Escape))
        //{
        //}
    }

    public void CambiarPan()
    {
        P0.CambiarPan();
        if (P0.Pan == true)
            pan.SetActive(true);
        else
            pan.SetActive(false);
    }

    public void CambiarSalsa()
    {
        P0.CambiarSalsa();
        if (P0.Salsa == true)
            salsa.SetActive(true);
        else
            salsa.SetActive(false);
    }

    public void CambiarQueso()
    {
        P0.CambiarQueso();
        if (P0.Queso == true)
            queso.SetActive(true);
        else
            queso.SetActive(false);
    }

    public void CambiarPepperoni()
    {
        P0.CambiarPepperoni();
        if (P0.Pepperoni == true)
            pepperoni.SetActive(true);
        else
            pepperoni.SetActive(false);
    }

    public void CambiarTocino()
    {
        P0.CambiarTocino();
        if (P0.Tocino == true)
            tocino.SetActive(true);
        else
            tocino.SetActive(false);
    }

    public void CambiarJalapeno()
    {
        P0.CambiarJalapeno();
        if (P0.Jalapeno == true)
            jalapeno.SetActive(true);
        else
            jalapeno.SetActive(false);
    }

    public void CambiarPimiento()
    {
        P0.CambiarPimiento();
        if (P0.Pimiento == true)
            pimento.SetActive(true);
        else
            pimento.SetActive(false);
    }

    public void CambiarChampinones()
    {
        P0.CambiarChampinones();
        if (P0.Champinones == true)
            champinones.SetActive(true);
        else
            champinones.SetActive(false);
    }

    public void CambiarPina()
    {
        P0.CambiarPina();
        if (P0.Pina == true)
            pina.SetActive(true);
        else
            pina.SetActive(false);
    }

    public void CambiarSalchicha()
    {
        P0.CambiarSalchicha();
        if (P0.Salchicha == true)
            salchicha.SetActive(true);
        else
            salchicha.SetActive(false);
    }

    public void CambiarJamon()
    {
        P0.CambiarJamon();
        if (P0.Jamon == true)
            jamon.SetActive(true);
        else
            jamon.SetActive(false);
    }

    public void CambiarAceitunas()
    {
        P0.CambiarAceitunas();
        if (P0.Aceitunas == true)
            aceitunas.SetActive(true);
        else
            aceitunas.SetActive(false);
    }

    public void Comparar()
    {
        bool aproved = true;

        if (P0.Pan != PComparar.Pan)
            aproved = false;
        if (P0.Salsa != PComparar.Salsa)
            aproved = false;
        if (P0.Queso != PComparar.Queso)
            aproved = false;
        if (P0.Pepperoni != PComparar.Pepperoni)
            aproved = false;
        if (P0.Tocino != PComparar.Tocino)
            aproved = false;
        if (P0.Pimiento != PComparar.Pimiento)
            aproved = false;
        if (P0.Champinones != PComparar.Champinones)
            aproved = false;
        if (P0.Jamon != PComparar.Jamon)
            aproved = false;
        if (P0.Pina != PComparar.Pina)
            aproved = false;
        if (P0.Salchicha != PComparar.Salchicha)
            aproved = false;
        if (P0.Jalapeno != PComparar.Jalapeno)
            aproved = false;
        if (P0.Aceitunas != PComparar.Aceitunas)
            aproved = false;

        if (aproved == true)
        {
            Contador++;
        }

        if (P0.Pan)
        {
            P0.CambiarPan();
            pan.SetActive(false);
        }
        if (P0.Salsa)
        {
            P0.CambiarSalsa();
            salsa.SetActive(false);
        }   
        if (P0.Queso)
        {
            P0.CambiarQueso();
            queso.SetActive(false);
        }
        if (P0.Pepperoni)
        {
            P0.CambiarPepperoni();
            pepperoni.SetActive(false);
        }
        if (P0.Tocino)
        {
            P0.CambiarTocino();
            tocino.SetActive(false);
        }
        if (P0.Pimiento)
        {
            P0.CambiarPimiento();
            pimento.SetActive(false);
        }
        if (P0.Champinones)
        {
            P0.CambiarChampinones();
            champinones.SetActive(false);
        }
        if (P0.Jamon)
        {
            P0.CambiarJamon();
            jamon.SetActive(false);
        }
        if (P0.Pina)
        {
            P0.CambiarPina();
            pina.SetActive(false);
        }
        if (P0.Salchicha)
        {
            P0.CambiarSalchicha();
            salchicha.SetActive(false);
        }
        if (P0.Jalapeno)
        {
            P0.CambiarJalapeno();
            jalapeno.SetActive(false);
        }
        if (P0.Aceitunas)
        {
            P0.CambiarAceitunas();
            aceitunas.SetActive(false);
        }

        CambioPizza();
    }

    public void CambioPizza()
    {
        op = 0;

        if (PComparar == P1)
        {
            op = 1;
            goto select;
        }
        if (PComparar == P2)
        {
            op = 2;
            goto select;
        }
        if (PComparar == P3)
        {
            op = 3;
            goto select;
        }
        if (PComparar == P4)
        {
            op = 4;
            goto select;
        }
        if (PComparar == P5)
        {
            op = 5;
            goto select;
        }
    select:
        switch (op)
        {
            case 0:
                PComparar = P2;
                break;
            case 1:
                PComparar = P2;
                break;
            case 2:
                PComparar = P3;
                break;
            case 3:
                PComparar = P4;
                break;
            case 4:
                PComparar = P5;
                break;
            case 5:
                PComparar = P1;
                break;
        }

    }
}
