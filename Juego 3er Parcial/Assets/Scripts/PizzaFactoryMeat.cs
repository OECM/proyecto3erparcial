﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PizzaFactoryMeat
{
    public Pizza crea()
    {
        Pizza _pizza = creaPizza();
        _pizza.Nombre = "Super Meaty";
        _pizza.Pan = true;
        _pizza.Salsa = true;
        _pizza.Queso = true;
        
        return _pizza;
    }

    protected abstract Pizza creaPizza();
}
