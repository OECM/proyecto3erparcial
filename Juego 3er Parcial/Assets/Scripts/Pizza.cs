﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pizza
{
    private string nombre;
    private bool pepperoni;
    private bool salsa;
    private bool pan;
    private bool queso;
    private bool tocino;
    private bool jalapeno;
    private bool pimiento;
    private bool champinones;
    private bool pina;
    private bool salchicha;
    private bool jamon;
    private bool aceitunas;

    public Pizza()
    {

    }

    public string Nombre
    {
        get { return nombre; }
        set { nombre = value; }
    }

    public bool Pepperoni
    {
        get { return pepperoni; }
        set { pepperoni = value; }
    }

    public bool Salsa
    {
        get { return salsa; }
        set { salsa = value; }
    }

    public bool Pan
    {
        get { return pan; }
        set { pan = value; }
    }

    public bool Queso
    {
        get { return queso; }
        set { queso = value; }
    }

    public bool Tocino
    {
        get { return tocino; }
        set { tocino = value; }
    }

    public bool Jalapeno
    {
        get { return jalapeno; }
        set { jalapeno = value; }
    }

    public bool Pimiento
    {
        get { return pimiento; }
        set { pimiento = value; }
    }

    public bool Champinones
    {
        get { return champinones; }
        set { champinones = value; }
    }

    public bool Pina
    {
        get { return pina; }
        set { pina = value; }
    }

    public bool Salchicha
    {
        get { return salchicha; }
        set { salchicha = value; }
    }

    public bool Jamon
    {
        get { return jamon; }
        set { jamon = value; }
    }

    public bool Aceitunas
    {
        get { return aceitunas; }
        set { aceitunas = value; }
    }
}
