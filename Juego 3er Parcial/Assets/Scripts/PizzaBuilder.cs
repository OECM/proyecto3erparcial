﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PizzaBuilder
{
    private Pizza pizza;

    public PizzaBuilder(string nombre)
    {
        pizza = new Pizza();
        pizza.Nombre = nombre;
    }

    public PizzaBuilder setPepperoni()
    {
        pizza.Pepperoni = !pizza.Pepperoni;
        return this;
    }

    public PizzaBuilder setSalsa()
    {
        pizza.Salsa = !pizza.Salsa;
        return this;
    }

    public PizzaBuilder setPan()
    {
        pizza.Pan = !pizza.Pan;
        return this;
    }

    public PizzaBuilder setQueso()
    {
        pizza.Queso = !pizza.Queso;
        return this;
    }

    public PizzaBuilder setTocino()
    {
        pizza.Tocino = !pizza.Tocino;
        return this;
    }

    public PizzaBuilder setJalapeno()
    {
        pizza.Jalapeno = !pizza.Jalapeno;
        return this;
    }

    public PizzaBuilder setPimiento()
    {
        pizza.Pimiento = !pizza.Pimiento;
        return this;
    }

    public PizzaBuilder setChampinones()
    {
        pizza.Champinones = !pizza.Champinones;
        return this;
    }

    public PizzaBuilder setPina()
    {
        pizza.Pina = !pizza.Pina;
        return this;
    }

    public PizzaBuilder setSalchicha()
    {
        pizza.Salchicha = !pizza.Salchicha;
        return this;
    }

    public PizzaBuilder setJamon()
    {
        pizza.Jamon = !pizza.Jamon;
        return this;
    }

    public PizzaBuilder setAceitunas()
    {
        pizza.Aceitunas = !pizza.Aceitunas;
        return this;
    }

    public Pizza build()
    {
        return pizza;
    }
}
