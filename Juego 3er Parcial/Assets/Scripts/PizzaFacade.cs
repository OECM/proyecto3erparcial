﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PizzaFacade : Pizza
{ 
    public void CambiarPan()
    {
        Pan = !Pan;
    }

    public void CambiarSalsa()
    {
        Salsa = !Salsa;
    }

    public void CambiarQueso()
    {
        Queso = !Queso;
    }

    public void CambiarPepperoni()
    {
        Pepperoni = !Pepperoni;
    }

    public void CambiarTocino()
    {
        Tocino = !Tocino;
    }

    public void CambiarJalapeno()
    {
        Jalapeno = !Jalapeno;
    }

    public void CambiarPimiento()
    {
        Pimiento = !Pimiento;
    }

    public void CambiarChampinones()
    {
        Champinones = !Champinones;
    }

    public void CambiarPina()
    {
        Pina = !Pina;
    }

    public void CambiarSalchicha()
    {
        Salchicha = !Salchicha;
    }

    public void CambiarJamon()
    {
        Jamon = !Jamon;
    }

    public void CambiarAceitunas()
    {
        Aceitunas = !Aceitunas;
    }
}
