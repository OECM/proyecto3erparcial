﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PizzaMeaty : PizzaFactoryMeat
{
    protected override Pizza creaPizza()
    {
        Pizza nuevaPizza = new Pizza();
        nuevaPizza.Jamon = true;
        nuevaPizza.Pepperoni = true;
        nuevaPizza.Tocino = true;
        nuevaPizza.Salsa = true;
        nuevaPizza.Salchicha = true;

        return nuevaPizza;
    }
}
